adonthell (0.3.8-2) unstable; urgency=high

  * Team upload.
  * Rebuild against python3.10 as default.
  * debian/control:
    + Bump Standards-Version to 4.6.0.
    + Bump debhelper compat to v13.

  [ Graham Inggs ]
  * debian/patches/python3.10.patch: Drop unused node.h include which
    is no longer in Python 3.10.

 -- Boyuan Yang <byang@debian.org>  Mon, 28 Mar 2022 13:31:59 -0400

adonthell (0.3.8-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.3.8.
  * Declare compliance with Debian Policy 4.3.0.
  * Use canonical VCS URI.

 -- Markus Koschany <apo@debian.org>  Sat, 12 Jan 2019 15:36:01 +0100

adonthell (0.3.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.3.7.
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.1.4.

 -- Markus Koschany <apo@debian.org>  Wed, 25 Apr 2018 00:05:24 +0200

adonthell (0.3.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Switch to compat level 10.
  * Tighten versioned recommendation on adonthell-data.

 -- Markus Koschany <apo@debian.org>  Tue, 27 Sep 2016 19:10:56 +0200

adonthell (0.3.6~beta1-1) unstable; urgency=medium

  * Team upload.
  * Moved the package to Git.
  * debian/control:
    - Remove dpkg-dev and versioned constraints from Build-Depends.
    - Remove libaa1-dev from Build-Depends.
    - Switch from SDL1 to SDL2.
    - Switch from Python 2 to Python 3.
    - Remove obsolete Breaks and change Recommends to
      adonthell-data (>= 0.3.6~beta1)
  * Remove README.debian and README.source.
  * Simplify debian/rules for new release.
  * Drop all patches. Fixed upstream.
  * Use --disable-pyc configure option.
  * Update docs file. FULLSCREEN.howto is no longer available.
  * Remove obsolete dirs file.
  * debian/watch: Use version=4.
  * Switch to copyright format 1.0.

 -- Markus Koschany <apo@debian.org>  Tue, 12 Jul 2016 21:31:34 +0200

adonthell (0.3.5-11) unstable; urgency=medium

  * Team upload.
  * Add disable-swig.patch and stop rebuilding adonthell.py from source due to
    incompatible changes in Swig >= 3 that would cause runtime errors on
    startup and make the game unusable. See README.source for more information
    and work in progress.
  * Add typemap-bool.patch but disable it by default. (See README.source)
  * Add format-security.patch to fix FTBFS due to use of format-security flag.
  * Build-Depend on dh-python and dh-autoreconf.
  * Build with parallel and autoreconf.

 -- Markus Koschany <apo@debian.org>  Thu, 14 Apr 2016 20:04:46 +0200

adonthell (0.3.5-10) unstable; urgency=medium

  * Team upload.

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org.

  [ Moritz Muehlenhoff ]
  * Remove myself from uploaders.

  [ Markus Koschany ]
  * wrap-and-sort -sa.
  * Declare compliance with Debian Policy 3.9.8.
  * Use compat level 9 and require debhelper >= 9.
  * Vcs-Browser: Use https.
  * Fix Lintian warning copyright-refers-to-symlink-license.
  * Try to build adonthell on all architectures again. Set architecture field
    to any. (Closes: #727144)
  * debian/rules: Remove empty directories.
  * Remove lintian-overrides file.

 -- Markus Koschany <apo@debian.org>  Mon, 11 Apr 2016 22:36:13 +0200

adonthell (0.3.5-9) unstable; urgency=low

  * Remove extra flag from --with python2. (Closes: #710317).

 -- Barry deFreese <bdefreese@debian.org>  Wed, 29 May 2013 14:44:36 -0400

adonthell (0.3.5-8) unstable; urgency=low

  * Acknowledge NMU.
  * Clean up packaging.
    - Change debian/rules to dh7 style.
    - Bump debhelper and compat to 7.
    - Replace Conflicts with Breaks in control.
    - Add hardening flags.
    - Update Homepage to new address.
    - Remove links against libdl and libutil since they aren't used.
  * Bump Standards Version to 3.9.4.

 -- Barry deFreese <bdefreese@debian.org>  Mon, 20 May 2013 08:24:40 -0400

adonthell (0.3.5-7.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/patches/12_no_sparc_bytecompile.patch:
    - Do not bytecompile Python files during build on sparc, workaround
      to let adonthell compile again. Generated files were not installed
      in the package, and byte-compilation is performed at a later stage
      by python-support anyway (Closes: #639450).

 -- Luca Falavigna <dktrkranz@debian.org>  Sun, 02 Oct 2011 17:21:03 +0200

adonthell (0.3.5-7) unstable; urgency=low

  * Team upload.
  * Thanks Torsten for the NMU!
  * Fix FTBFS caused by the NMU.
    configure looks for a "swig" binary, the swig2.0 package provides
    just swig2.0, making the build fail. Build-Depend on swig (>= 1.3),
    which will pull in the latest stable swig by default.
    Closes: #633107, #630933, #632531

 -- Evgeni Golov <evgeni@debian.org>  Fri, 08 Jul 2011 16:10:50 +0200

adonthell (0.3.5-6.1) unstable; urgency=low

  * Non-maintainer upload.
  * Build with swig2.0 to allow removal of the obsolete swig1.3 package.

 -- Torsten Landschoff <torsten@debian.org>  Wed, 22 Jun 2011 23:46:34 +0200

adonthell (0.3.5-6) unstable; urgency=low

  * Team upload.

  [ Iain Lane ]
  * debian/control: Remove myself from Uploaders

  [ Peter De Wachter ]
  * Fixed FTBFS with newer gcc. (Closes: #624998) (LP: #765984)
    - Added patch: 11_ftbfs_with_gcc-4.6.diff

  [ Evgeni Golov ]
  * Drop the "A" from the Description.
  * Add ${misc:Depends} to Depends.

 -- Evgeni Golov <evgeni@debian.org>  Sun, 08 May 2011 22:55:59 +0200

adonthell (0.3.5-5) unstable; urgency=low

  * Drop support for armel for now (Closes: #562904)

 -- Moritz Muehlenhoff <jmm@debian.org>  Wed, 06 Jan 2010 19:26:27 +0100

adonthell (0.3.5-4) unstable; urgency=low

  * Add myself to uploaders
  * Drop special case handling of Python for arm, Python 2.4 is being
    removed. If it still fails with the current toolchain, we need to
    find a fix in Python or drop support for Arm (Closes: #562401)
  * Drop versioned Build-Depends for python-dev, swig1.3 and
    python-support; all satified even in Etch
  * Convert to source format 3 (quilt)

 -- Moritz Muehlenhoff <jmm@debian.org>  Thu, 24 Dec 2009 16:31:20 +0100

adonthell (0.3.5-3) unstable; urgency=low

  [ Barry deFreese ]
  * 10_ftbfs_with_gcc-4.4.diff. Fix FTBFS with gcc-4.4. (Closes: #546356).
    + Thanks to Ilya Barygin for the patch!
  * Add README.source for quilt.
  * Bump Standards Version to 3.8.3. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Sat, 12 Dec 2009 09:01:51 -0500

adonthell (0.3.5-2) unstable; urgency=low

  [ Barry deFreese ]
  * Update my e-mail address.
  * Bump Standards Version to 3.8.2. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Tue, 28 Jul 2009 11:46:07 -0400

adonthell (0.3.5-1) experimental; urgency=low

  [ Gonéri Le Bouder ]
  * add the missing -p1 in debian/patches/series so the package doesn't
    FTBFS with the quilt source format (Closes: #484977)
  * Bump the standard version to 3.8.0, no change needed.

  [ Ansgar Burchardt ]
  * Add watch file

  [ Iain Lane ]
  * New upstream version 0.3.5
  * debian/rules: Use quilt.make to apply patches.
  * debian/patches: Delete obsolete patches.
  * debian/patches/04_do_not_ship_pyc.diff: Refreshed.
  * debian/watch: Updated to look at savannah.nongnu.org

  [ Miriam Ruiz ]
  * Added Iain Lane <laney@ubuntu.com> to Uploaders

 -- Iain Lane <laney@ubuntu.com>  Wed, 31 Dec 2008 14:23:46 +0000

adonthell (0.3.4.cvs.20080529-1+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS on arm and armel, thanks to peter green. (Closes: #486654)

 -- Philipp Kern <pkern@debian.org>  Fri, 01 Aug 2008 11:08:10 +0200

adonthell (0.3.4.cvs.20080529-1) unstable; urgency=low

  [ Barry deFreese ]
  * New maintainer. (Closes: #427141).
    + Debian Games Team <pkg-games-devel@lists.alioth.debian.org>.
    + Add myself to uploaders.
  * New upstream CVS snapshot.
    + Drop all current patches as they are fixed in CVS.
    + Fixes to build with python2.5. (Closes: #451631).
  * Make clean not ignore errors.
  * Add copyright holders to debian/copyright.
  * Move Homepage from package description to source header.
  * Add VCS tags.
  * Add autotools-dev build dependency.
  * Don't ship pyc files. (Closes: #392492).
    + Thanks to Luis Rodrigo Gallardo Cruz for the patch.
  * Remove symlink and rename binary to /usr/games/adonthell.
  * Bump debian/compat to 5.
  * Bump Standards Version to 3.7.3. (No changes needed).

  [ Gonéri Le Bouder ]
  * Remove src/modules/adonthell.py src/py_adonthell_wrap.cc since they are
    generated with SWIG during the build process

 -- Barry deFreese <bddebian@comcast.net>  Thu, 29 May 2008 21:54:50 -0400

adonthell (0.3.4.cvs.20050813-4) unstable; urgency=medium

  * QA upload.
  * Apply patch from Peter Green to fix FTBFS with gcc 4.3.  closes: #477011.
  * Bump to Standards-Version 3.7.3.

 -- Clint Adams <schizo@debian.org>  Mon, 21 Apr 2008 10:39:20 -0400

adonthell (0.3.4.cvs.20050813-3) unstable; urgency=low

  * Orphan package, set maintainer to Debian QA Group

 -- Gordon Fraser <gordon@debian.org>  Sat, 02 Jun 2007 07:18:26 +0200

adonthell (0.3.4.cvs.20050813-2.4) unstable; urgency=high

  * Non-maintainer upload.
  * Replace remaining "<SDL/SDL_keysym.h>" with <SDL_keysym.h>.
    (Closes: #398694)
    Thanks Goswin von Brederlow and Matthew Johnson for noticing.

 -- Mohammed Adnène Trojette <adn+deb@diwi.org>  Sat, 18 Nov 2006 17:43:05 +0100

adonthell (0.3.4.cvs.20050813-2.3) unstable; urgency=low

  * Non-maintainer upload.
  * Fix SDL include statement in src/py_adonthell.i.

 -- Mohammed Adnène Trojette <adn+deb@diwi.org>  Wed, 20 Sep 2006 00:27:19 +0200

adonthell (0.3.4.cvs.20050813-2.2) unstable; urgency=medium

  * Non-maintainer upload with medium urgency.
  * Update to the New Python Policy (Closes: #380756)
     + debian/control
        - bump build-dependency on debhelper to (>= 5.0.37.2)
        - remove build-dependency on python and python-dev
        - build-depend on python-dev (>= 2.3.5-11)
        - build-depend on python-support (>= 0.4)
        - depend on ${python:Depends}
     + debian/rules
        - build with $(shell pyversions -d) to make the package bin-NMUable
        - run dh_pythonsupport only (hence the >= 0.4 build-dep)
  * Build-Depend on libsdl1.2-dev, libsdl-ttf2.0-dev and libsdl-mixer1.2-dev instead
    of with the embedded version, add -lSDL_ttf to --with-py-libs in debian/rules
    and rebootstrap (Closes: #382202)
  * Work around #374062 using a patch from Julien Danjou (Closes: #381456)
  * Build-Depend on quilt for patch management and add adequate rules.
  * Bump Standards-Version to 3.7.2.

 -- Mohammed Adnène Trojette <adn+deb@diwi.org>  Thu, 10 Aug 2006 20:47:56 +0200

adonthell (0.3.4.cvs.20050813-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Remove obsolete internal objects include (Closes: #370147)

 -- Mohammed Adnène Trojette <adn+deb@diwi.org>  Tue,  6 Jun 2006 10:21:05 +0200

adonthell (0.3.4.cvs.20050813-2) unstable; urgency=low

  * Fix build problem with g++ 4.1 (Closes: #356171)

 -- Gordon Fraser <gordon@debian.org>  Wed,  5 Apr 2006 15:03:17 +0200

adonthell (0.3.4.cvs.20050813-1) unstable; urgency=low

  * Change dependency to libaa1-dev (Closes: #320890)
  * Update to latest CVS (Closes: #297096, #315305)

 -- Gordon Fraser <gordon@debian.org>  Sat, 13 Aug 2005 09:18:48 +0200

adonthell (0.3.3.cvs.20031022-3) unstable; urgency=medium

  * Fix problem with SWIG macro changes (Closes: #297096)
  * Urgency set to medium due to fix for RC bug

 -- Gordon Fraser <gordon@debian.org>  Thu,  3 Mar 2005 18:08:27 +0100

adonthell (0.3.3.cvs.20031022-2) unstable; urgency=low

  * gcc-3.4 compile fixes (Closes: #262996, #263404, #249730)

 -- Gordon Fraser <gordon@debian.org>  Wed, 11 Aug 2004 15:33:44 +0200

adonthell (0.3.3.cvs.20031022-1) unstable; urgency=low

  * New CVS checkout
  * Fix python include path (Closes: #216484)
  * Standards version to 3.6.1
  * Include upstream homepage in description

 -- Gordon Fraser <gordon@debian.org>  Wed, 22 Oct 2003 16:36:30 +0200

adonthell (0.3.3.cvs.20030119-2) unstable; urgency=low

  * Rebuild against new libvorbis0a package (Closes: #184976)

 -- Gordon Fraser <gordon@debian.org>  Tue, 25 Mar 2003 11:57:26 +0100

adonthell (0.3.3.cvs.20030119-1) unstable; urgency=low

  * New CVS snapshot:
    - fixes g++ 3.2 compile problems (Closes: #177307)
  * Bumped standards-version to 3.5.8
  * Removed build-dependency on bison

 -- Gordon Fraser <gordon@debian.org>  Sun, 19 Jan 2003 16:14:39 +0100

adonthell (0.3.3-2) unstable; urgency=low

  * Revert str_hash.c changes to fix build problem on hppa

 -- Gordon Fraser <gordon@debian.org>  Tue,  1 Oct 2002 07:25:13 +0200

adonthell (0.3.3-1) unstable; urgency=low

  * New upstream release

 -- Gordon Fraser <gordon@debian.org>  Thu, 19 Sep 2002 16:41:22 +0200

adonthell (0.3.2.cvs.20020906-3) unstable; urgency=low

  * Remove accidential need for autotools (Closes: #160665)

 -- Gordon Fraser <gordon@debian.org>  Fri, 13 Sep 2002 08:33:43 +0200

adonthell (0.3.2.cvs.20020906-2) unstable; urgency=low

  * Remove -fno-rtti to fix build problem on ia64
  * Revert hash<string> changes to fix build problem on hppa

 -- Gordon Fraser <gordon@debian.org>  Thu, 12 Sep 2002 19:47:21 +0200

adonthell (0.3.2.cvs.20020906-1) unstable; urgency=low

  * Initial Debian-upload (Closes: #158679)

 -- Gordon Fraser <gordon@debian.org>  Wed, 28 Aug 2002 20:37:34 +0200
